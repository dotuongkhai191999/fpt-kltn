import 'react-native-gesture-handler';
import React, {useState} from 'react';
import LoadPage_01 from '../Loading/LoadPage_01';
import LoadPage_02 from '../Loading/LoadPage_02';
import ChooseTestPage from '../Loading/ChooseTestPage';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import {GoogleLogin} from '../Login/GoogleLogin';
import LoginPage from '../Login';
import HomePage from '../Home';
import EditProfile from '../Profile/Edit';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from '../Home/DrawerContent';
import {LogBox} from 'react-native';
import Intro01 from '../Introduce/intro_01';
import Intro02 from '../Introduce/intro_02';
import Intro03 from '../Introduce/intro_03';
import ResultHistory from '../History/resultHistory';
import InputGrade from '../Login/inputGrade';
import InputGPA from '../Login/inputGPA';
import TestDraft01 from '../DoTest/Dtest01';
import UnitMBTI from '../DoTest/UnitMBTI';
import ActiveUser from '../Login/active';
import PersonalResult from '../Result/personalResult';
import MajorResult from '../Result/majorResult';
import ResultHistoryDetail from '../History/resultHistoryDetail';
import ChooseColleges from '../Colleges/ChooseColleges';
import Notificate from '../Home/noti';
import WishList from '../Colleges/WishList';
import AllColleges from '../AboutUs/about';
import Chat from '../AdmissionsCounseling/chat';
import Counseling from '../AdmissionsCounseling/counseling';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AnimTab1 from './test';

LogBox.ignoreLogs(['Reanimated 2']);
LogBox.ignoreLogs(['EventEmitter.removeListener']);
LogBox.ignoreAllLogs();
const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();
const StackNavigate = ({navigation, route}) => {
  return (
    <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen
        options={{headerShown: false}}
        name="Load01"
        component={LoadPage_01}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Load02"
        component={LoadPage_02}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ChooseTestPage"
        component={ChooseTestPage}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="HomeScreen"
        component={HomePage}
        initialParams={route.params}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="UniversityScreen"
        component={ChooseColleges}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="EditProfile"
        component={EditProfile}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="AllColleges"
        component={AllColleges}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ResultHistory"
        component={ResultHistory}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ResultHistoryDetail"
        component={ResultHistoryDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="DoTest01"
        component={TestDraft01}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="DoTest02"
        component={UnitMBTI}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="personalityGrResult"
        component={PersonalResult}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="MajorResult"
        component={MajorResult}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="InputGrade"
        component={InputGrade}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="InputGPA"
        component={InputGPA}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Notificate"
        component={Notificate}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="WishList"
        component={WishList}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Counseling"
        component={Counseling}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Chat"
        component={Chat}
      />
    </Stack.Navigator>
  );
};
const DrawerObj = ({navigation, route}) => {
  //console.log(route.params);
  return (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen
        // nhận params từ login gửi qua
        initialParams={route.params}
        name="DrawerController"
        options={{headerShown: false}}
        component={StackNavigate}
      />
    </Drawer.Navigator>
  );
};

{
  /*design tab chuyển trang ở dưới*/
}

const Tab = createMaterialBottomTabNavigator();

export default function Routes() {
  // return (
  //   <NavigationContainer>
  //     <AnimTab1 />
  //   </NavigationContainer>
  // );
  return (
    <NavigationContainer>
      {/* LoginPage MainHome*/}
      <Stack.Navigator initialRouteName="Intro01">
        <Stack.Screen
          options={{headerShown: false}}
          name="Intro01"
          component={Intro01}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Intro02"
          component={Intro02}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Intro03"
          component={Intro03}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="LoginPage"
          component={LoginPage}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Activate"
          component={ActiveUser}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="MainHome"
          component={DrawerObj}
          // AnimTab1
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="mainBoard"
          component={AnimTab1}
          // AnimTab1
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
