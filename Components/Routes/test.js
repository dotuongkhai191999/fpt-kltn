import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React, {useEffect, useRef} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AllColleges from '../AboutUs/about';
import chat from '../AdmissionsCounseling/chat';
import WishList from '../Colleges/WishList';
import HomePage from '../Home';
import ChooseTestPage from '../Loading/ChooseTestPage';
import EditProfile from '../Profile/Edit';

const Tab = createBottomTabNavigator();
const animate1 = {
  0: {scale: 0.5, translateY: 7},
  0.92: {translateY: -15},
  1: {scale: 1.2, translateY: -10},
};

const animate2 = {
  0: {scale: 1.2, translateY: -10},
  1: {scale: 1, translateY: 7},
};

const circle1 = {
  0: {scale: 0},
  0.3: {scale: 0.9},
  0.5: {scale: 0.2},
  0.8: {scale: 0.7},
  1: {scale: 1},
};

const circle2 = {0: {scale: 1}, 1: {scale: 0}};

const TabButton = props => {
  const {item, onPress, accessibilityState} = props;
  const focused = accessibilityState.selected;
  const viewRef = useRef(null);
  const circleRef = useRef(null);
  const textRef = useRef(null);

  useEffect(() => {
    if (focused) {
      viewRef.current.animate(animate1);
      circleRef.current.animate(circle1);
      textRef.current.transitionTo({scale: 1});
    } else {
      viewRef.current.animate(animate2);
      circleRef.current.animate(circle2);
      textRef.current.transitionTo({scale: 0});
    }
  }, [focused]);

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={styles.container}>
      <Animatable.View ref={viewRef} duration={200} style={styles.container}>
        <View style={styles.btn}>
          <Animatable.View ref={circleRef} style={styles.circle} />
          <MaterialCommunityIcons
            name={item.icon}
            style={{fontSize: 20}}
            color={focused ? 'white' : 'black'}
          />
        </View>
        <Animatable.Text ref={textRef} style={styles.text}>
          {item.label}
        </Animatable.Text>
      </Animatable.View>
    </TouchableOpacity>
  );
};

export default function AnimTab1({navigation, route}) {
  const TabArr = [
    {
      route: 'account',
      label: 'Thông tin cá nhân',
      icon: 'account',
      component: EditProfile,
    },
    {
      route: 'Testing',
      label: 'Kiểm tra',
      icon: 'archive-edit',
      component: ChooseTestPage,
    },
    {
      route: 'Home',
      label: 'Trang chủ',
      icon: 'home',
      component: HomePage,
      initialParams: route.params,
    },
    {
      route: 'Chatting',
      label: 'Tư vấn tuyển sinh',
      icon: 'chat',
      component: chat,
    },
    {
      route: 'CollegeList',
      label: 'Danh sách trường',
      icon: 'school',
      component: AllColleges,
    },
  ];
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
        tabBarStyle: styles.tabBar,
      }}>
      {TabArr.map((item, index) => {
        return (
          <Tab.Screen
            initialParams={route.params}
            key={index}
            name={item.route}
            component={item.component}
            options={{
              tabBarShowLabel: false,
              tabBarButton: props => <TabButton {...props} item={item} />,
            }}
          />
        );
      })}
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabBar: {
    height: 80,
    // borderRadius: 16,
    elevation: 10,
    shadowColor: 'black',
    shadowOpacity: 1,
    width: '100%',
    flexWrap: 'wrap',
  },
  btn: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'white',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    borderRadius: 25,
  },
  text: {
    fontSize: 10,
    textAlign: 'center',
    color: 'black',
    marginHorizontal: 5,
  },
});
