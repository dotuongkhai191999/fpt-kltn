import {View, Text, Dimensions, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken} from '../Constant-Storage';
import API from '../API/api';

export default function CloneHolland({navigation, route}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const ScreenWidth = Dimensions.get('screen').width;
  useEffect(() => {
    const getPersonGroupName = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `personality_groups`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            console.log(res.data);
            res.data.map((item, index) => {
              if (item.personalityGroupId === personGroupid) {
                setPersonGrName(item);
              }
            });
          }
        })
        .catch(err => alert(err));
    };
    getPersonGroupName();
  }, []);
  return (
    <View style={{flex: 1, position: 'relative', backgroundColor: '#ECA376'}}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: ScreenHeight * 0.15,
          width: ScreenWidth,
          borderBottomEndRadius: 20,
          borderBottomStartRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: 'white',
          marginBottom: 30,
        }}>
        <Text style={{fontSize: 20}}>PersonalResult</Text>
      </View>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: ScreenHeight * 0.6,
          borderRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: 'white',
          marginLeft: 10,
          marginRight: 10,
        }}>
        {/* <Text style={{fontSize: 20}}>Điểm bạn đạt được là {point}</Text>*/}
        <Text style={{fontSize: 20, paddingBottom: 20}}>
          Đây là kết quả tính cách phù hợp với bạn : S
        </Text>
        <Text style={{fontSize: 20, textAlign: 'center'}}>
          Những người thích làm việc với mọi người để khai sáng, truyền kiến
          thức, giúp đỡ, đào tạo, hoặc chữa bệnh cho họ, hoặc có kỹ năng sử dụng
          ngôn từ.
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate('fake')}
        style={{
          height: 50,
          width: 300,
          backgroundColor: '#00ffff',
          position: 'absolute',
          bottom: 10,
          right: 50,
          borderRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontSize: 22,
            color: 'white',
            textAlign: 'center',
          }}>
          Xem nghề phù hợp với bạn
        </Text>
      </TouchableOpacity>
    </View>
  );
}
