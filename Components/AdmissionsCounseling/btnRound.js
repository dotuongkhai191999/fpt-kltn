/**
 * Dispays the round button with icon
 * You can set size, icon, icon color, button color, icon size etc.
 */

 import React from 'react';
 import { TouchableOpacity } from 'react-native';
 import Icon from 'react-native-vector-icons/Ionicons';
 
 const BtnRound = (props) => {
   return (
     <TouchableOpacity 
       style={[
         {
           height: props.size || 30,
           width: props.size || 30,
           borderRadius: props.size || 30,
           backgroundColor: props.color || "#CDC2AE",
           alignItems: 'center',
           justifyContent: 'center',
           backfaceVisibility: 'hidden',
         },
         props.style
       ]}
       activeOpacity={0.7}
       onPress={props.onPress}
     >
       {
         props.customIcon 
          ? (props.customIcon) 
          : (<Icon solid={props.solid} name={props.icon} size={props.iconSize || 18} color={props.iconColor || "CDC2AE"} />)
       }
     </TouchableOpacity>
   )
 }
 
 BtnRound.defaultProps = {
   size: 30,
  //  color: Colors.cartButtonColor,
   onPress: null,
   customIcon: null,
   iconSize: 18,
  //  iconColor: Colors.primary,
   solid: false,
   style: null
 };
 
 export default BtnRound