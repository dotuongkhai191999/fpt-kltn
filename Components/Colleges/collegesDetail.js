import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {white} from 'react-native-paper/lib/typescript/styles/colors';

export default function CollegesDetail() {
  return (
    <View style={{flex: 1, position: 'relative'}}>
      <TouchableOpacity
        onPress={{}}
        style={{
          height: 40,
          width: 40,
          backgroundColor: 'white',
          elevation: 50,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          position: 'absolute',
          left: 0,
          top: 0,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Feather name="arrow-left" size={35} color="orange" />
      </TouchableOpacity>
      <View>
        <Image
          style={{width: '100%', height: 250}}
          source={require('../../assets/Intro01.jpg')}
        />
      </View>
      <View style={{paddingHorizontal: 10, paddingTop: 20}}>
        <View>
          <Text
            style={{
              textAlign: 'left',
              color: 'black',
              fontSize: 25,
              fontWeight: 'bold',
            }}>
            Đại học kinh tế
          </Text>
          <Text
            style={{
              textAlign: 'left',
              color: 'black',
              fontSize: 20,
              paddingTop: 15,
            }}>
            Là một đại học đa ngành tại Việt Nam, nằm trong nhóm đại học trọng
            điểm quốc gia. Trường cũng là một trong những trường đại học đào tạo
            kinh tế, kinh doanh quản lý thuộc top đầu Việt Nam.
          </Text>
        </View>
        <View>
          <Text
            style={{
              textAlign: 'left',
              color: 'black',
              fontSize: 25,
              fontWeight: 'bold',
              paddingTop: 10,
            }}>
            Địa chỉ:
          </Text>
          <Text style={{color: 'black', fontSize: 20}}>
            59C Nguyễn Đình Chiểu, Phường 6, Quận 3, Thành phố Hồ Chí Minh
          </Text>
        </View>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View
          style={{
            justifyContent: 'center',
            height: 50,
            width: 180,
            marginTop: 20,
            marginLeft: 15,
            borderRadius: 20,
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            backgroundColor: '#ABD9FF',
          }}>
          <TouchableOpacity
            onPress={{}}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'black', fontSize: 20}}>Website trường</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            justifyContent: 'center',
            height: 50,
            width: 180,
            marginTop: 20,
            marginLeft: 15,
            borderRadius: 20,
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            backgroundColor: '#ABD9FF',
          }}>
          <TouchableOpacity
            onPress={{}}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'black', fontSize: 20}}>Liên hệ tư vấn</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
