import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Button,
} from 'react-native';
import React, {useState, useRef, useEffect} from 'react';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';

export default function TestPage_01({navigation, route}) {
  const [dataQuestion, setDataQuestion] = useState(route.params.dataQuestion);
  const scrollHolland = useRef();
  const tempRender = dataQuestion.slice(0, 10);
  // const dataRender = dataQuestion.slice(0, 10);
  const [dataRender, setDataRender] = useState(tempRender);
  const [dataResult, setDataResult] = useState([]);
  const chooseStar = (index, star, indexQuestion) => {
    const temp = [...star];
    const tempResult = [...dataRender];
    var i, j;
    for (i = 0; i <= index; i++) {
      temp[i] = true;
    }

    for (j = star.length - 1; j > index; j--) {
      temp[j] = false;
    }
    tempResult[indexQuestion].star = temp;
    setDataRender([...tempResult]);
    // const name = await AsyncStorage.getItem(keys.User_ProfLink);
  };
  const NextBtnPress = async () => {
    
  };

  return (
    <>
      <Image
        source={require('../../assets/HollandDescription.png')}
        resizeMode="contain"
        style={{width: 400, height: 200}}
      />
      <ScrollView ref={scrollHolland}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
            flex: 1,
          }}>
          {dataRender.map((it, i) => {
            return (
              <View
                key={i}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '90%',
                }}>
                <Text
                  style={{
                    fontWeight: '300',
                    fontSize: 18,
                    width: '100%',
                  }}>
                  <Text style={{fontWeight: '500', fontSize: 20}}>
                    {i + 1} &nbsp;
                  </Text>
                  {it.title}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  {it.star.map((item, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        onPress={() => chooseStar(index, it.star, i)}>
                        <AntDesign
                          name={item ? 'star' : 'staro'}
                          size={25}
                          color={item ? 'orange' : 'black'}
                        />
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            );
          })}

          <TouchableOpacity
            style={{
              backgroundColor: 'red',
              marginVertical: 20,
              height: 50,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => NextBtnPress()}>
            <Text style={{color: 'white', fontSize: 20}}>Next</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
}
