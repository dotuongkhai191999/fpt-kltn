import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Button,
} from 'react-native';
import React, {useState, useRef, useEffect} from 'react';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import {render} from 'react-native/Libraries/Renderer/implementations/ReactNativeRenderer-prod';

export default function TestPage_02({navigation, route}) {
  const [dataQuestion, setDataQuestion] = useState(route.params.dataQuestion);
  // const scrollMBTI = useRef();
  const tempRender = dataQuestion.slice(0, 10);
  //const dataRender = dataQuestion.slice(0, 10);
  const [dataRender, setDataRender] = useState(tempRender);
  const [dataResult, setDataResult] = useState([]);
  const chooseAnswer = (index, indexQuestion) => {
    const temp = [...dataRender];
    temp[indexQuestion].chooseAnswer = index;
    setDataRender(temp);
    // const name = await AsyncStorage.getItem(keys.User_ProfLink);
  };
  const NextBtnPress = async () => {
    const temp = [...dataQuestion];
    for (var k = 0; k < 10; k++) temp.shift();
    // const resultTemp = [];
    const tempRender = temp.slice(0, 10);

    setDataResult([...dataResult, ...dataRender]);
    if (dataResult.length >= 10) {
      console.log('du r');
    } else {
      setDataQuestion([...temp]);
      setDataRender([...tempRender]);
      //scrollMBTI.current.scrollTo({x: 0, y: 0, animated: false});
    }
  };

  return (
    <>
      {/* <Image
        source={require('../../assets/HollandDescription.png')}
        resizeMode="contain"
        style={{width: 400, height: 200}}
      /> */}
      <ScrollView>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
            flex: 1,
          }}>
          {dataRender.map((it, i) => {
            return (
              <View
                key={i}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '90%',
                }}>
                <Text
                  style={{
                    fontWeight: '300',
                    fontSize: 18,
                    width: '100%',
                    justifyContent: 'center'
                  }}>
                  <Text style={{fontWeight: 'bold', fontSize: 20, color: 'black'}}>
                    {i + 1} &nbsp;
                  </Text>
                  {it.title}
                </Text>
                <View style={{flexDirection: 'column', alignSelf: 'flex-start'}}>
                  {it.answers.map((item, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        onPress={() => chooseAnswer(index, it.answers, i)}>
                        <Text
                          style={{
                            color: index === it.answers ? 'red' : 'black', fontSize: 18
                          }}>{item}</Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            );
          })}

          <TouchableOpacity
            style={{
              backgroundColor: 'red',
              marginVertical: 20,
              height: 50,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => NextBtnPress()}>
            <Text style={{color: 'white', fontSize: 20}}>Next</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
}
