import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken} from '../Constant-Storage';
import API from '../API/api';
import UnitMBTI from './UnitMBTI';
export default function TestDraft02({navigation, route}) {
  const questionID = route.params;
  return (
    <View style={{position: 'relative'}}>
      <UnitMBTI id={questionID} />
    </View>
  );
}
