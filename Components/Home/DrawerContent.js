import {
  ImageBackground,
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import {Drawer} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {NavigationContainer} from '@react-navigation/native';
import {useNavigation} from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken, avtURL, userFullName} from '../Constant-Storage';
import {GoogleSignin} from '@react-native-google-signin/google-signin';

export default function DrawerContent(props) {
  const navigation = useNavigation();
  const signOut = async () => {
    //console.log('dang xuat');
    try {
      GoogleSignin.configure({
        webClientId:
          'firebase-adminsdk-iyo41@personalstrength-83af0.iam.gserviceaccount.com',
      });
      // const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      // setloggedIn(false);
      // setuserInfo([]);
      await AsyncStorage.clear();
      navigation.navigate('LoginPage');
    } catch (err) {
      alert('loi log out' + err);
      await AsyncStorage.clear();
      navigation.navigate('LoginPage');
    }
  };

  const [displayAvatar, setDisplayAvatar] = useState('');
  const [displayUserName, setDisplayUserName] = useState('');

  useEffect(() => {
    async function userInfo() {
      const userAvatar = await AsyncStorage.getItem(avtURL);
      const userName = await AsyncStorage.getItem(userFullName);
      setDisplayAvatar(userAvatar);
      setDisplayUserName(userName);
    }
    userInfo();
  });

  //if (Object.keys(introUser).length !== 0)
  return (
    <View
      style={{
        flex: 1,
        flexWrap: 'nowrap',
        // backgroundColor: 'transparent',
      }}>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{backgroundColor: '#ECA376'}}>
        {/* design khung ảnh header */}
        <ImageBackground
          style={{height: 200, padding: 20}}
          source={require('../../assets/DrawerTopBanner.jpg')}>
          <Image
            style={{width: 100, height: 100, borderRadius: 50}}
            source={{
              uri: displayAvatar
                ? displayAvatar
                : 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
            }}
          />
          <Text
            style={{
              color: '#fff',
              fontSize: 25,
              fontFamily: 'Roboto-Medium',
              marginBottom: 5,
            }}>
            {displayUserName}
          </Text>
        </ImageBackground>
        {/* design tab chuyển trang */}
        <View style={{padding: 5, backgroundColor: '#fff'}}>
          {/*nút quay về home*/}
          <TouchableOpacity
            onPress={() => navigation.navigate('HomeScreen')}
            style={{
              height: 50,
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: 5,
              elevation: 20,
              shadowOffset: {width: 10, height: 10},
              shadowColor: 'black',
              shadowOpacity: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
            }}>
            <View style={{alignSelf: 'center', marginLeft: 10}}>
              <FontAwesome5 name="home" size={20} color="#ECA376" />
            </View>
            <Text style={{fontSize: 20, marginLeft: 16, alignSelf: 'center'}}>
              Trang chủ
            </Text>
          </TouchableOpacity>
          {/*nút edit profile*/}
          <TouchableOpacity
            onPress={() => navigation.navigate('EditProfile')}
            style={{
              height: 50,
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: 5,
              elevation: 20,
              shadowOffset: {width: 10, height: 10},
              shadowColor: 'black',
              shadowOpacity: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
            }}>
            <View style={{alignSelf: 'center', marginLeft: 10}}>
              <FontAwesome5 name="user-edit" size={20} color="#ECA376" />
            </View>
            <Text style={{fontSize: 20, marginLeft: 10, alignSelf: 'center'}}>
              Thông tin cá nhân
            </Text>
          </TouchableOpacity>
          {/*nút xem Result history*/}
          <TouchableOpacity
            onPress={() => navigation.navigate('ResultHistory')}
            style={{
              height: 50,
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: 5,
              elevation: 20,
              shadowOffset: {width: 10, height: 10},
              shadowColor: 'black',
              shadowOpacity: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
            }}>
            <View style={{alignSelf: 'center', marginLeft: 10}}>
              <FontAwesome5 name="history" size={20} color="#ECA376" />
            </View>
            <Text style={{fontSize: 20, marginLeft: 16, alignSelf: 'center'}}>
              Lịch sử kết quả
            </Text>
          </TouchableOpacity>
          {/*nút xem lịch sử nguyện vọng*/}
          <TouchableOpacity
            onPress={() => navigation.navigate('WishList')}
            style={{
              height: 50,
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: 5,
              elevation: 20,
              shadowOffset: {width: 10, height: 10},
              shadowColor: 'black',
              shadowOpacity: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
            }}>
            <View style={{alignSelf: 'center', marginLeft: 10}}>
              <MaterialIcons name="history-edu" size={20} color="#ECA376" />
            </View>
            <Text style={{fontSize: 20, marginLeft: 16, alignSelf: 'center'}}>
              Nguyện vọng
            </Text>
          </TouchableOpacity>
          {/* nút xem about us */}
          <TouchableOpacity
            onPress={() => navigation.navigate('Counseling')}
            style={{
              height: 50,
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: 5,
              elevation: 20,
              shadowOffset: {width: 10, height: 10},
              shadowColor: 'black',
              shadowOpacity: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
            }}>
            <View style={{alignSelf: 'center', marginLeft: 10}}>
              <FontAwesome5 name="info-circle" size={20} color="#ECA376" />
            </View>
            <Text style={{fontSize: 20, marginLeft: 16, alignSelf: 'center'}}>
              Tư vấn
            </Text>
          </TouchableOpacity>         
        </View>
      </DrawerContentScrollView>
      {/*nút log out*/}
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({color, size}) => (
            <Icon name="exit-to-app" color={color} size={size} />
          )}
          label="Đăng xuất"
          labelStyle={{fontSize: 20, fontWeight: 'bold'}}
          onPress={signOut}
        />
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    //paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  btnInforTitle: {
    flex: 1,
    paddingVertical: 10,
    textAlign: 'left',
    paddingLeft: 10,
    color: 'black',
    fontSize: 20,
    fontWeight: '900',
  },
  btnDropDownTitle: {
    fontSize: 20,
    fontWeight: '900',
    paddingLeft: 0,
  },
});
