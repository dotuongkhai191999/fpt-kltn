import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  BackHandler,
  Alert,
  Linking,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {SafeAreaView} from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import API from '../API/api';

export default function HomeMainBoard() {
  const [visible, setVisible] = useState(false);
  const [avtToDisplay, setAvtToDisplay] = useState('');
  const [displayName, setdisplayName] = useState('');

  useEffect(() => {
    return () => {
      setVisible(false);
    };
  }, []);

  useEffect(() => {
    async function test() {
      const token = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      api
        .onCallAPI(
          'get',
          'sys_users/profile',
          {},
          {},
          {Authorization: 'bearer ' + token},
        )
        .then(res => {
          if (res) {
            const avtLink = res.data.imagePath;
            const userTemp = res.data.fullName;
            const displayTemp = userTemp.slice(
              userTemp.lastIndexOf(' '),
              userTemp.length,
            );
            setAvtToDisplay(avtLink);
            setnameToDisplay(userTemp);
            setdisplayName(displayTemp);
          }
        })
        .catch(err => console.log(err));
    }
    test();
  });

  return (
    <View style={{flex: 3, position: 'relative'}}>
      {/*design header */}
      <SafeAreaView style={{paddingBottom: 15, paddingTop: 20}}>
        <View
          style={{
            paddingHorizontal: 15,
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <View style={{flex: 3, alignItems: 'flex-start'}}>
            <TouchableOpacity
              onPress={() => {
                navigation.toggleDrawer();
              }}
              style={{flex: 1}}>
              <Feather name="align-left" size={35} color="orange" />
            </TouchableOpacity>
          </View>
          <View style={{alignItems: 'flex-end'}}>
            <TouchableOpacity onPress={{}} style={{flex: 1}}>
              <Entypo name="bell" size={35} color="orange" />
            </TouchableOpacity>
            {/* thiết kế cho chuông thông báo */}
          </View>
          {/*view avartar*/}
          <Image
            style={{width: 70, height: 70, borderRadius: 35, marginLeft: 15}}
            source={require('../../assets/Intro01.jpg')}
          />
          {/* {avtToDisplay.length > 0 ? (
            <Image
              source={{
                uri: avtToDisplay
                  ? avtToDisplay
                  : 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
              }}
              resizeMode="cover"
              style={{width: '100%', height: '100%', borderRadius: 40}}
            />
          ) : (
            <></>
          )} */}
        </View>
        <View style={{paddingLeft: 15, paddingTop: 10}}>
          {/*view name*/}
          <View style={{justifyContent: 'flex-start'}}>
            <Text
              style={{
                textAlign: 'left',
                color: 'black',
                fontSize: 25,
                fontWeight: 'bold',
              }}>
              Xin chào {displayName}
            </Text>
            <Text
              style={{
                textAlign: 'left',
                color: 'black',
                fontSize: 20,
              }}>
              Hãy cùng chúng tôi hiểu hơn về khả năng của bạn
            </Text>
          </View>
        </View>
      </SafeAreaView>
      <View style={{paddingHorizontal: 15}}>
        {/*design ds trường đề cử*/}
        <View>
          <Text
            style={{
              textAlign: 'left',
              color: 'black',
              fontSize: 22,
              fontWeight: 'bold',
              paddingBottom: 10,
            }}>
            Những trường đang hot
          </Text>
        </View>
        <View>
          <ScrollView horizontal style={{flexDirection: 'row'}}>
            <View
              style={{
                backgroundColor: 'white',
                height: 120,
                width: 180,
                borderRadius: 20,
                elevation: 50,
                shadowOffset: {width: 5, height: 5},
                shadowColor: 'black',
                shadowOpacity: 1,
                margin: 5,
              }}></View>
            <View
              style={{
                backgroundColor: 'white',
                height: 120,
                width: 180,
                borderRadius: 20,
                elevation: 50,
                shadowOffset: {width: 5, height: 5},
                shadowColor: 'black',
                shadowOpacity: 1,
                margin: 5,
              }}></View>
            <View
              style={{
                backgroundColor: 'white',
                height: 120,
                width: 180,
                borderRadius: 20,
                elevation: 50,
                shadowOffset: {width: 5, height: 5},
                shadowColor: 'black',
                shadowOpacity: 1,
                margin: 5,
              }}></View>
          </ScrollView>
          {/*design ds khóa học đề cử*/}
          <View>
            <Text
              style={{
                textAlign: 'left',
                color: 'black',
                fontSize: 22,
                fontWeight: 'bold',
                paddingBottom: 10,
                paddingTop: 10,
              }}>
              Các khóa học hấp dẫn
            </Text>
          </View>
          <ScrollView horizontal style={{flexDirection: 'row'}}>
            <View
              style={{
                backgroundColor: 'white',
                height: 150,
                width: 200,
                borderRadius: 20,
                elevation: 50,
                shadowOffset: {width: 5, height: 5},
                shadowColor: 'black',
                shadowOpacity: 1,
                margin: 5,
              }}></View>
            <View
              style={{
                backgroundColor: 'white',
                height: 150,
                width: 200,
                borderRadius: 20,
                elevation: 50,
                shadowOffset: {width: 5, height: 5},
                shadowColor: 'black',
                shadowOpacity: 1,
                margin: 5,
              }}></View>
            <View
              style={{
                backgroundColor: 'white',
                height: 150,
                width: 200,
                borderRadius: 20,
                elevation: 50,
                shadowOffset: {width: 5, height: 5},
                shadowColor: 'black',
                shadowOpacity: 1,
                margin: 5,
              }}></View>
          </ScrollView>
          {/*design bottom bar*/}
        </View>
      </View>
      <TouchableOpacity
        onPress={{}}
        style={{
          height: 70,
          width: 70,
          borderRadius: 35,
          backgroundColor: 'white',
          elevation: 50,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          position: 'absolute',
          right: 10,
          top: 320,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <Entypo name="chat" size={45} color="orange" />
      </TouchableOpacity>
    </View>
  );
}
