import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  Modal,
  StyleSheet,
  FlatList,
  Pressable
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import API from '../API/api';
import {LoginAppToken} from '../Constant-Storage';
import Spiner from '../Loading/Spiner';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function AllColleges({navigation}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const Screenwidth = Dimensions.get('screen').width;
  const [listAllColleges, setListAllColleges] = useState([]);
  const [pageNumber, setPageNumber] = useState('');
  const [pageSize, setPageSize] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [connectors, setConnectors] = useState([])
  const [loading, isLoading] = useState(false)

  if (loading) return <Spiner />;

  const getConnectors = async (collegeId) => {
    // isLoading(true)
    const bearerToken = await AsyncStorage.getItem(LoginAppToken)
    const api = new API()
    await api.onCallAPI(
      'get',
      `colleges/${collegeId}/connector`,
      {},
      {},
      { Authorization: 'bearer ' + bearerToken },
    ).then(res => {
      setConnectors([...res.data])
      setModalVisible(true)
    }).catch(err => {
      alert(err)
    }).finally(() => isLoading(false))
  }

  useEffect(() => {
    const getAllColleges = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `colleges/Dashboard`,
          {},
          {PageNumber: 1, PageSize: 10},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            console.log(res.data);
            setListAllColleges(res.data.college.data);
          }
        })
        .catch(err => alert(err));
    };
    getAllColleges();
  }, []);
  const listAllCollegesRender = () => {
    if (!listAllColleges) return <Spiner />;
    return listAllColleges.map((item, index) => {
      // const supportedURL = item.data.referenceLink;
      return (
        <View
          key={index}
          style={{
            backgroundColor: 'white',
            height: ScreenHeight * 0.25,
            marginVertical: 5,
            borderRadius: 20,
            elevation: 10,
            shadowOffset: {width: 10, height: 10},
            shadowColor: 'black',
            shadowOpacity: 1,
            marginHorizontal: 5,
            justifyContent: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Image
                source={{
                  uri: item.imagePath,
                }}
                resizeMode="cover"
                style={{
                  height: 80,
                  width: 80,
                  borderRadius: 40,
                  marginLeft: 10,
                }}
              />
            </View>
            <View style={{flex: 2, marginRight: 10}}>
              <Text style={{fontSize: 20}}>{item.collegeName}</Text>
              <Text style={{fontSize: 20}}>
                Địa chỉ:
                {item.address}
              </Text>
              <TouchableOpacity 
                onPress={() => getConnectors(item.collegeId)}
              >
                <Text style={{fontSize: 20}}>Bấm vào đây để xem điểm</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    });
  };
  const linkRender = () => {
    const handlePress = async supportedURL => {
      const supported = await Linking.canOpenURL(supportedURL);
      if (supported) {
        await Linking.openURL(supportedURL);
      } else {
        Alert.alert(`Don't know how to open this URL: ${supportedURL}`);
      }
    };
  };
  const renderBtnConnectors = ({ item, index }) => (
    <TouchableOpacity
      style={{
        width: Screenwidth,
        height: 50,
        backgroundColor: ((index % 2) === 0) ? '#DAE5D0' : '#F9EBC8',
        justifyContent: 'center'
      }}
      onPress={async () => {
        const bearerToken = await AsyncStorage.getItem(LoginAppToken)
        await new API().onCallAPI(
          'post',
          `sys_users/${item.accountId}/openchat`,
          {},
          {},
          { Authorization: 'bearer ' + bearerToken },
        ).then(res => {
          const roomId = res.data.id
          navigation.navigate('Chat', {roomId: roomId})
        }).catch(err => {
          alert(err)
        }).finally(() => isLoading(false))
        setModalVisible(false)
      }}
    >
      <Text style={{ textAlign: 'center', fontSize: 20, color: 'black' }}>{`${item.accountId} : ${item.name}`}</Text>
    </TouchableOpacity>
  )


  return (
    <View style={{flex: 1, position: 'relative'}}>
      {/*Design header*/}
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          height: 190,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          justifyContent: 'center',
        }}>
        <SafeAreaView style={{paddingBottom: 10}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 0.5, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{flex: 1}}>
                <Ionicons name="arrow-back-outline" size={32} color="white" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 32,
                color: 'white',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              DANH SÁCH TRƯỜNG
            </Text>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
        </SafeAreaView>
      </LinearGradient>
      {/*Design body*/}
      <Modal
        animationType='slide'
        visible={modalVisible}
      >
        <View style={styles.centeredView}>
          <Text style={{
            fontSize: 22,
            textDecorationLine: 'underline',
            marginBottom: 20
          }}>Danh sách tư vấn viên</Text>
          <FlatList
            data={connectors}
            renderItem={renderBtnConnectors}
            keyExtractor={item => item.accountId}
          />
          <Pressable 
            style={[styles.button, styles.buttonClose]}
            onPress={() => setModalVisible(false)}>
            <Text>Close</Text>
          </Pressable>
        </View>
      </Modal>
      <View
        style={{
          borderTopLeftRadius: 50,
          borderTopRightRadius: 50,
          overflow: 'hidden',
          flex: 1,
        }}>
        <ScrollView
          style={{
            backgroundColor: 'white',
            marginTop: 20,
            elevation: 20,
            shadowOffset: {width: 10, height: 10},
            shadowColor: 'black',
            shadowOpacity: 1,
            paddingHorizontal: 15,
          }}>
          {listAllCollegesRender()}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  button: {
    width: 200,
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    // justifyContent: 'center',
    alignItems: 'center'
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
})
